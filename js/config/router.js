import {DrawerNavigator,StackNavigator} from 'react-navigation';
import React, { Component } from 'react';
import { Platform,Dimensions } from 'react-native';
// import Splash from '../screens/Splash';
import Login from '../screens/Login';
import Signup from '../screens/Signup';
import Home from '../screens/Home';
import Books from '../screens/Books';
import Magazines from '../screens/Magazines';
import Magazinesdetails from '../screens/Magazinesdetails';
import Audio from '../screens/Audio';
import Audiodetails from '../screens/Audiodetails';
import Bookdetails from '../screens/Bookdetails';
import Cart from '../screens/Cart';
import Confirmorder from '../screens/Confirmorder';
import Order from '../screens/Order';
import Orderdetails from '../screens/Orderdetails';
import Category from '../screens/Category';
import Changepassword from '../screens/Changepassword';
import Profile from '../screens/Profile';
import Payments from '../screens/Payments';
import Splash from '../screens/Splash';
import Forgotpassword from '../screens/Forgotpassword';
import Video from '../screens/Video';
import Videodetails from '../screens/Videodetails';
import Shipping from '../screens/Shipping';
import Sidebar from '../screens/Sidebar';
const screenWidth = Dimensions.get("window").width;
const screenHeight = Dimensions.get("window").height;
const Drawer=(
  {
 Home:{screen:Home},
 Audio:{screen:Audio},
 Audiodetails:{screen:Audiodetails},
 Login:{screen:Login},
 Signup:{screen:Signup},
 Books:{screen:Books},
 Magazines:{screen:Magazines},
 Magazinesdetails:{screen:Magazinesdetails},

 Bookdetails:{screen:Bookdetails},
 Cart:{screen:Cart},
 Confirmorder:{screen:Confirmorder},
 Category:{screen:Category},
 Changepassword:{screen:Changepassword},
 Profile:{screen:Profile},
 Payments:{screen:Payments},
 Forgotpassword:{screen:Forgotpassword},
 Shipping:{screen:Shipping},
 Order:{screen:Order},
 Orderdetails:{screen:Orderdetails},
 Video:{screen:Video},
 Videodetails:{screen:Videodetails},
  });
  const NavigationMenu = DrawerNavigator(
    Drawer,
    {
      initialRouteName:'Home',
      drawerWidth: screenWidth - (Platform.OS === 'android' ? 56 : (screenWidth >414 ? 500 : 64)),
      contentComponent: props => <Sidebar {...props} routes={Drawer}/>,
      drawerPosition:'left',
    });
const StackNav=StackNavigator(
    {
      Splash:{screen:Splash},
      NavigationMenu:{screen:NavigationMenu},      
    },
  {headerMode:'none'});
  export default StackNav;