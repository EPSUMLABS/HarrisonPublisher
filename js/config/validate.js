export const validateEmail = (mail)=>{
    if (/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/.test(mail)) {
       return (true)
    } 
     return (false)
   }
   export const validateName = (name)=>{
    if (/^[A-Za-z\s]+$/.test(name)) {
       return (true)
    } 
     return (false)
   }
   export const validatePhone = (phone)=>{
    if (/^\+?([0-9]{2})\)?[-. ]?([0-9]{4})[-. ]?([0-9]{4})$/.test(phone)) {
       return (true)
    } 
     return (false)
   }
   export const validatePassword = (password)=>{
    if (/(?=.*\d)(?=.*[a-z])/.test(password)) {
       return (true)
    } 
     return (false)
   }
   export const ValidatePincode=(zip)=>
   {
     if(/^[1-9][0-9]{5}$/.test(zip))
     {
       return(true)
     }
     return(false)
   }
   
   export const ValidateNumber=(num)=>
   {
     if(/^[0-9]+$/.test(num))
     {
       return(true)
     }
     return(false)
   }
   
   export const ValidatePan=(pan)=>
   {
     if(/^([a-zA-Z]){5}([0-9]){4}([a-zA-Z]){1}?$/.test(pan))
     {
       return(true)
     }
     return(false)
   }
   
   export const validateAddress = (address)=>{
    if (/^[a-z0-9\s,'-]*$/.test(address)) {
       return (true)
    } 
     return (false)
   }
   
