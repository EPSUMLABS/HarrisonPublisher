/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */

import React, { Component } from 'react';
import {
  Platform,
  StyleSheet,
  Text,
  View,Dimensions,TouchableOpacity,Image,StatusBar,AsyncStorage,BackHandler
} from 'react-native';
import {
    Container, Header, Left, Body, Right, Icon, Button, Title, Footer, FooterTab, Content, Thumbnail,
     List, ListItem, Tab, Tabs, ScrollableTab,
    Separator,Picker,Form,Textarea,
    Item, Input, Spinner,Card, CardItem
} from 'native-base';
import Star from '../config/Star'
const screenwidth = Dimensions.get('window').width;
const screenHeight = Dimensions.get('window').height;
import urldetails from '../config/endpoints.json';
import showRating from 'react-native-ratings';
import Modal1 from 'react-native-simple-modal';
export default class Orderdetails extends Component {
    static defaultProps = {
        defaultRating: 3,
        reviews: ["Terrible", "Bad", "Okay", "Good", "Great"],
        count: 5,
        onFinishRating: () => console.log('Rating selected'),
        showRating: true
       
    };

        state = {
            orderdetails: this.props.navigation.state.params.orderdetails,
            open1: false,
            position: '',
            reason:"",
            review: '',
            spinner:true,
          
              
        }
      
      componentWillMount(){
    }
    componentDidMount() {
        BackHandler.addEventListener('hardwareBackPress', this.onBackPress);
        const { defaultRating } = this.props

        this.setState({ position: defaultRating })
        console.log(this.state.position)
    }
    renderStars(rating_array) {
        return _.map(rating_array, (star, index) => {
            return star
        })
    }

    componentWillUnmount() {
      BackHandler.removeEventListener('hardwareBackPress', this.onBackPress);
    }
    onBackPress = () => {
      this.props.navigation.navigate("Home");
      return true;
    };
    modalDidOpen1 = () => console.log("Modal did open.");
    modalDidClose1 = () => {
        this.setState({ open1: false });
        console.log("Modal did close.");
    };
    openModal1 = () => this.setState({ open1: true });
    closeModal1 = () => this.setState({ open1: false });
    cancel(orderid) {
        // this.setState({ spinner: true })
            var formData = new FormData();
            formData.append('order_id', this.state.orderid);
            fetch(urldetails.base_url + urldetails.url["ordercancel"], {
                method: "POST",
                body: formData,
            })
                .then((response) => response.json())
                .then((jsondata) => {
                    // console.log(jsondata)
                    if (jsondata.status === "success") {
                        console.log(success)
                        // alert(jsondata.message)
                        this.setState({spinner:false})
                    }
                    else {
                        alert(jsondata.message)
                    }
                    this.setState({ open: false, spinner: false })
                    this.props.navigation.navigate('Order')
                })
            console.log("my order id " + orderid )
        
    }
    starSelectedInPosition(position) {
        const { onFinishRating } = this.props

        onFinishRating(position);
        this.setState({ position: position })
        console.log(this.state.position)
    }
    Feedback(orderid) {
        this.setState({ spinner: true })
        if (this.state.review == "") {
            console.log(this.state.review)
            alert('Please Enter the review')
        }
        else if (this.state.position == "") {
            alert('Please give the ratings !')
        }
        else {
            var formData = new FormData();
            formData.append('order_id', orderid);
            formData.append('review_content', this.state.review);
            formData.append('review_star', this.state.position);
            fetch(urldetails.base_url + urldetails.url["orderreview"], {
                method: "POST",
                body: formData,
            })
                .then((response) => response.json())
                .then((jsondata) => {
                    console.log(jsondata)
                    if (jsondata.status === "success") {
                        alert(jsondata.message)
                    }
                    else {
                        alert(jsondata.message)
                    }
                    this.setState({ open1: false, spinner: false })
                    this.props.navigation.navigate('Orders')
                })
                }
    }
  render() {
    const { position } = this.state
    const { count, reviews, showRating } = this.props
    const rating_array = []

    _.times(count, index => {
        rating_array.push(
            <Star
                key={index}
                position={index + 1}
                starSelectedInPosition={this.starSelectedInPosition.bind(this)}
                fill={position >= index + 1}
                {...this.props}
            />
        )
    })

    return (
     <Container>
         
         <Header style={{ backgroundColor: '#094872' }} hasTabs>
                    <Left>
                        <TouchableOpacity transparent onPress={() => this.props.navigation.openDrawer()}>
                            <Icon name='ios-menu' style={{ fontSize: 25, color: 'white' }} />
                        </TouchableOpacity>
                    </Left>
                    <Body><Title>Order</Title></Body>
                </Header>
                <StatusBar backgroundColor="#000" barStyle="light-content" />
                 <Content>   
                {/* {this.state.spinner ? <View style={{marginTop:screenHeight/3}}><Spinner color='#ffb400' /><Text style={{color:'#000',textAlign:'center'}}>Loading...</Text></View> : <View />}        */}
                <Card>
                        <CardItem>
                            <Text style={{ fontSize: 16 }}>ORDER ID :  </Text>
                            <Text style={{ fontSize: 16, color: "#ffb400" }}>{this.state.orderdetails.oid}</Text>
                        </CardItem>
                        <CardItem>
                            <Text style={{ fontSize: 16 }}>STATUS : </Text>
                            <Text style={{ fontSize: 16 }}>{this.state.orderdetails.tstatus}</Text>
                        </CardItem>
                        <CardItem>
                            <Text style={{ fontSize: 16 }}>PLACED ON : </Text>
                            <Text style={{ fontSize: 16 }}>{this.state.orderdetails.on_date}</Text>
                        </CardItem>
                        <List>
                            <ListItem>
                                <Left>
                                    <Text style={{ fontSize: 16 }}>Product id</Text>
                                </Left>
                                <Left>
                                    <Text style={{ fontSize: 16, marginLeft: 40 }}>Qty</Text>
                                </Left>
                                <Left>
                                    <Text style={{ fontSize: 16, marginLeft: 40 }}>Price</Text>
                                </Left>
                            </ListItem>
                        </List>
                        <List dataArray={this.state.orderdetails.cart}
                            renderRow={(myitem) =>
                                <ListItem>
                                    <Left>
                                        <Text style={{ fontSize: 16 }}>{myitem.product_id}</Text>
                                    </Left>
                                    <Left>
                                        <Text style={{ fontSize: 16, marginLeft: 40 }}>{myitem.quantity}</Text>
                                    </Left>
                                    <Left>
                                        <Text style={{ fontSize: 16, marginLeft: 40 }}>{myitem.price}</Text>
                                    </Left>
                                </ListItem>
                            }>
                        </List>
                        <CardItem>
                            <Left><Text style={{ fontSize: 16 }}>GST :</Text></Left>
                            <Right><Text></Text></Right>
                        </CardItem>
                        <CardItem>
                            <Left><Text style={{ fontSize: 16 }}>Delivery Charge :</Text></Left>
                            <Right><Text></Text></Right>
                        </CardItem>
                        <CardItem>
                            <Left><Text style={{ fontSize: 16 }}>Bill Amount :</Text></Left>
                            <Right><Text>{this.state.orderdetails.total_price}</Text></Right>
                        </CardItem>
                        <CardItem>
                            <Left><Text style={{ fontSize: 16 }}>Payment Mode :</Text></Left>
                            <Right><Text>COD</Text></Right>
                        </CardItem>
                        <TouchableOpacity style={{
                                    width: screenwidth - 20, height: 50, backgroundColor: "#e62e00", borderRadius: 5, marginLeft: 5, marginRight: 5, marginTop: 20,
                                    alignItems: "center", justifyContent: 'center', elevation: 8, shadowColor: 'black', shadowOffset: { width: 3, height: 3 }, shadowOpacity: 0.5, shadowRadius: 5
                                }}
                                onPress={() => this.cancel(this.state.orderdetails.oid)}>
                                    <Text style={{ color: "white", fontSize: 16 }}>Cancel Order</Text>
                                </TouchableOpacity>
                                {/* <TouchableOpacity style={{
                                width: screenwidth - 20, height: 50, backgroundColor: "#1f7a1f", borderRadius: 5, marginLeft: 5, marginRight: 5, marginTop:10,
                                alignItems: "center", justifyContent: 'center', elevation: 8, shadowColor: 'black', shadowOffset: { width: 3, height: 3 }, shadowOpacity: 0.5, shadowRadius: 5
                            }}
                                onPress={() => this.setState({ open1: true })}>
                                <Text style={{ color: "white", fontSize: 16 }}>Feedback</Text>
                            </TouchableOpacity>  */}
                    </Card>
    
                    {/* <TouchableOpacity style={{
                                    width: screenwidth - 20, height: 50, backgroundColor: "#e62e00", borderRadius: 5, marginLeft: 5, marginRight: 5, marginTop: 20, marginBottom: 40,
                                    alignItems: "center", justifyContent: 'center', elevation: 8, shadowColor: 'black', shadowOffset: { width: 3, height: 3 }, shadowOpacity: 0.5, shadowRadius: 5
                                }}
                                onPress={() => this.cancel(this.state.orderdetails.oid)}>
                                    <Text style={{ color: "white", fontSize: 16 }}>Cancel Order</Text>
                                </TouchableOpacity>
                                <TouchableOpacity style={{
                                width: screenwidth - 20, height: 50, backgroundColor: "#1f7a1f", borderRadius: 5, marginLeft: 5, marginRight: 5, marginTop: 20, marginBottom: 40,
                                alignItems: "center", justifyContent: 'center', elevation: 8, shadowColor: 'black', shadowOffset: { width: 3, height: 3 }, shadowOpacity: 0.5, shadowRadius: 5
                            }}
                                onPress={() => this.setState({ open1: true })}>
                                <Text style={{ color: "white", fontSize: 16 }}>Feedback</Text>
                            </TouchableOpacity>  */}


        </Content> 
        <Modal1
                    offset={0}
                    open={this.state.open1}
                    modalDidOpen={this.modalDidOpen1}
                    modalDidClose={this.modalDidClose1}
                    style={{ alignItems: "center" }}
                >
                    <View style={{ alignItems: 'center', justifyContent: 'center' }}>
                        <View style={{ backgroundColor: 'transparent', flexDirection: 'column', alignItems: 'center', justifyContent: 'center' }}>
                            {showRating &&
                                <Text style={{ fontSize: 25, fontWeight: 'bold', margin: 10, color: 'rgba(230, 196, 46, 1)' }}>
                                    {reviews[position - 1]}
                                </Text>
                            }
                            <View style={{ flexDirection: 'row', alignItems: 'center', justifyContent: 'center' }}>
                                {this.renderStars(rating_array)}
                            </View>
                        </View>

                        <Textarea style={{ width: '95%', textAlign: 'center' }} rowSpan={5} bordered placeholder="Write Your Review Here" onChangeText={(rev) => this.setState({ review: rev })} />
                        {this.state.spinner ? <View><Spinner color='#ffb400' /></View> : <View />}
                     

                        <TouchableOpacity onPress={() => this.Feedback(this.state.orderdetails.oid)} style={{
                            width: screenwidth - 75, height: 50, backgroundColor: "#1f7a1f", borderRadius: 5, marginLeft: 5, marginRight: 5, marginTop: 20, marginBottom: 40,
                            alignItems: "center", justifyContent: 'center', elevation: 8, shadowColor: 'black', shadowOffset: { width: 3, height: 3 }, shadowOpacity: 0.5, shadowRadius: 5
                        }}
                        >
                            <Text style={{ color: "white", fontSize: 20 }}>SUBMIT</Text>

                        </TouchableOpacity>
                    </View>
                </Modal1>

    

                     


   
        

        
         </Container>
    );
  }
}


