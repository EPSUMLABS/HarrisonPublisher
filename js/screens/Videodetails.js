
import React, { Component } from 'react';
import {
  Platform,
  StyleSheet,
  Text,
  View,ImageBackground,TouchableOpacity,Image,Dimensions,StatusBar,ScrollView
} from 'react-native';
import { Container, Header, Content,Left,Body,Title,Right,Item,Input,Thumbnail,Icon,Tab,Tabs,TabHeading,
  Card,ScrollableTab,List,CardItem,CheckBox,Badge,
Footer,FooterTab, ListItem} from 'native-base';
import urldetails from '../config/endpoints.json';
import ReadMore from 'react-native-read-more-text';
const screenwidth=Dimensions.get('window').width;
const screenHeight=Dimensions.get('window').height;
var SQLite = require('react-native-sqlite-storage');
let db = SQLite.openDatabase({ name: 'test.db', createFromLocation: "~Harrison.db", location: 'Library' }, this.openCB, this.errorCB);
export default class Videodetails extends Component {
  state={
    details: this.props.navigation.state.params.details,
  }
  componentWillMount(){
    this.fetchcartitem()
  }
   addtocart( p_id,name,price,p_hardcopy,p_softcopy) {
  // this._onpressadd(itemid);
     db.transaction((tx) => {
       tx.executeSql('insert into cart(p_id,p_name,p_price,qty,p_hardcopy,p_softcopy) values(?,?,?,?,?,?)', [p_id,name,price,1,p_hardcopy,p_softcopy], (tx, results) => {
         console.log("added to cart") 
       });
   });
   this.fetchcartitem()
   }
   fetchcartitem() {
    db.transaction((tx) => {
            tx.executeSql('SELECT * FROM cart', [], (tx, results) => {
                console.log("Query completed");
                lcart = [];
                item = 0;
                 t = 0;
                var len = results.rows.length; 
               for (let i = 0; i < len; i++) {
                    let row = results.rows.item(i);
                    lcart.push(row);
                    t = t + parseInt(row.qty * row.p_price);
                    item = i + 1;
                    this.setState({ cart: lcart, t_item: item, total: t })
                    console.log(this.state.cart) 
               }   
            });
      })
  };
   
  render() {
     
  return (
      <Container>
           <Header style={{ backgroundColor: '#094872' }} hasTabs>
                    <Left>
                        <TouchableOpacity onPress={()=>this.props.navigation.openDrawer()}>
                            <Icon name='ios-menu' style={{ fontSize: 25, color: 'white' }} />
                        </TouchableOpacity>
                    </Left>
                    <Body><Title>Details</Title></Body>
                    <Right>
                   
                    <Badge style={{ backgroundColor: 'red',borderRadius: 25,width: 22, height: 22,marginBottom:8 }}>
            <Text style={{ color: 'white',marginLeft:2 }}>{this.state.t_item}</Text>
            </Badge>    
                    <Icon name='ios-cart' style={{ fontSize: 25, color: 'white' }} />
                      </Right>
                </Header>
                <StatusBar backgroundColor="#1a1a1a" barStyle="light-content" />
          <Content style={{backgroundColor:'#e8e8e8'}}>
          <Card style={{height:screenHeight/3,marginTop:5}}>
          <View style={{flexDirection:'row'}}>
          <Image source={{uri: urldetails.image+this.state.details.videopath[0].sample_path}} style={{height:screenHeight/3.5, width:screenwidth-220,marginTop:15,marginLeft:10, }}/>
          <View style={{flexDirection:'column',marginLeft:10,marginTop:10,marginLeft:30}}>
                  <Text style={{fontSize:20,color:'#000'}}>{this.state.details.ptitle}</Text>
                  <Text style={{fontSize:15,paddingTop:5,color:'#000'}}>{this.state.details.pauther}</Text>
                  <Text style={{fontSize:15,}}>{this.state.details.ppublisher}</Text>
                  <Text style={{fontSize:15}}></Text>
                  <Text style={{fontSize:15}}></Text>
                  <View style={{flexDirection:'row',marginTop:20}}>
                  {/* <TouchableOpacity  style={{ backgroundColor: '#dadee5', width: screenwidth / 3.5 ,borderWidth:2,borderColor:'#094872' }}> */}
                <View style={{ flexDirection: 'row', alignContent: 'center', justifyContent: 'center', padding: 10 }}>
                 
                  <Text style={{  padding: 2, fontSize: 15 }}></Text>
                  <Text style={{ color: '#000', padding: 2, fontSize: 15 }}>Rs.{this.state.details.pprice}</Text>
                 
                </View>
              {/* </TouchableOpacity> */}
              </View>
              {/* <TouchableOpacity style={{marginTop:20,marginLeft:10,backgroundColor:'#9999ff',alignContent:'center',alignItems:'center'}}><Text
              style={{fontSize:17}}>download</Text></TouchableOpacity> */}
                  </View>      
          </View>
          </Card> 
          <List style={{marginTop:5,backgroundColor:'#fff'}}>
            <ListItem style={{flexDirection:'column'}}>
           <Text>ABOUT THE BOOK</Text>
           <ReadMore
            numberOfLines={2}
            onReady={this._handleTextReady}> 
            <Text style={{color:'black',fontSize:16}}>
             {this.state.details.pdescription}
            </Text>
          </ReadMore>
              </ListItem>
            </List>
      </Content>
      <Footer style={{paddingTop:0}}>
            <FooterTab style={{ backgroundColor: '#fff' }}>
              <TouchableOpacity onPress={()=>this.props.navigation.navigate('Payments')} 
               style={{ backgroundColor: '#094872', width: screenwidth / 2 - 0.2 }}>
                <View style={{ flexDirection: 'row', alignContent: 'center', justifyContent: 'center', padding: 10 }}>
                  {/* <FontAwesome style={{ color: '#fff', padding: 2,fontSize: 16 }} name="pencil-square-o" /> */}
                  <Text style={{ color: '#fff', padding: 2, fontSize: 16 }}>BUY</Text>
                  {/* <FontAwesome style={{ color: '#fff', paddingLeft: 2,fontSize: 16 }} name="pencil-square-o" /> */}
                </View>
              </TouchableOpacity>
              <TouchableOpacity   onPress={() => this.addtocart(this.state.details.pid,this.state.details.ptitle,this.state.details.pprice,
              this.state.details.p_hardcopy,this.state.details.p_softcopy)}
               style={{ backgroundColor: '#094872', width: screenwidth / 2 - 0.2 }}>
                <View style={{ flexDirection: 'row', alignContent: 'center', justifyContent: 'center', padding: 10 }}>
                  <Icon style={{ color: '#ffb400', padding: 2 }} name="md-cart" />
                  <Text style={{ color: '#fff', padding: 2, fontSize: 16 }}>Add to Cart</Text>
                </View>
              </TouchableOpacity>
            </FooterTab>
          </Footer>
  
     </Container>
    );
  }
}


