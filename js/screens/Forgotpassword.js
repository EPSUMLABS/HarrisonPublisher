import React, { Component } from 'react';
import { View, Text, TouchableOpacity, Dimensions, StatusBar, ImageBackground, AsyncStorage, BackHandler,
   Alert } from 'react-native';

import { Container, Header, Left, Body, Icon, Title, Content, Thumbnail, Item, Input, Spinner } from 'native-base';
import urldetails from '../config/endpoints.json';
const screenwidth = Dimensions.get('window').width;
const screenHeight = Dimensions.get('window').height;
export default class Forgotpassword extends Component {
  constructor() {
    super();
    this.state = {
      email: '',
      spinner: false,
    }
  }
  forgotpass() {
    if (this.state.email == "") {
      alert('Please Enter your Email..')
    } 
    else {
      this.setState({ spinner: true })
      var formData = new FormData();
      formData.append('cemail', this.state.email);
      console.log(formData)
      fetch(urldetails.base_url + urldetails.url["fpassword"], {
        method: "POST",
        body: formData
      })
        .then((response) => response.json())
        .then((jsondata) => {
          console.log(jsondata)
          if (jsondata.status == "success") {
            Alert.alert('', jsondata.message)
          }
          else {
            Alert.alert('', jsondata.msg)
          } this.setState({ spinner: false })
        })
    }
  }


  render() {
    return (
      <Container>
          <Header style={{ backgroundColor: "#094872" }} hasTabs>
            <Left>
              <TouchableOpacity transparent onPress={() => this.props.navigation.openDrawer()}>
                <Icon name='ios-menu' style={{ fontSize: 25, color: 'white' }} />
              </TouchableOpacity>
            </Left>
            <Body><Title></Title></Body>
          
          </Header>
          <StatusBar backgroundColor="#000" barStyle="light-content" />
          <Content>
            <View style={{ padding: 40, marginTop: screenHeight / 50, alignItems: 'center' }}>
              
              <Text style={{ textAlign: 'center', color: '#000', fontSize: 22, fontWeight: 'bold', marginTop: 20 }}>FORGOT PASSWORD</Text>
            </View>
            <View style={{ flexGrow: 1, alignItems: 'center', justifyContent: 'center', marginTop: 5 }}>
              <Item regular style={{ width: screenwidth - 40, alignItems: 'center', backgroundColor: '#fff', borderRadius: 5, borderColor: '#000', marginTop: 10 }}>
                <Input style={{ fontSize: 16, color: 'black' }} placeholder='EMAIL'  placeholderTextColor='#000' textAlign='center' onChangeText={(email) => this.setState({ email: email })} />
              </Item>
              <View style={{ height: 40, marginTop: 25, marginBottom: 20, justifyContent: 'center', alignItems: 'center', }}>
                <TouchableOpacity onPress={() => this.forgotpass()} style={{
                  borderRadius: 5, width: screenwidth - 40, height: 50, backgroundColor: "#094872",
                  alignItems: "center", justifyContent: 'center', elevation: 8, shadowColor: 'black', shadowOffset: { width: 3, height: 3 }, shadowOpacity: 0.5, shadowRadius: 5
                }}>
                  <Text style={{ color: "white", fontSize: 20 }}>SUBMIT</Text>
                </TouchableOpacity>
              </View>
             
            </View>
          </Content>
       
      </Container>
    );
  }
}
