import React, { Component } from 'react';
import {
  Platform,
  StyleSheet,
  Text,
  View,Image,Dimensions,AsyncStorage
} from 'react-native';
import { Container, Content,Spinner } from 'native-base';
import urldetails from '../config/endpoints.json';
const screenwidth = Dimensions.get('window').width;
const screenHeight = Dimensions.get('window').height;
var DeviceInfo = require('react-native-device-info');
export default class Splash extends Component {
  state = {
    spinner: false,
    token:'',
  };
  componentDidMount(){
              _.delay(() => this.props.navigation.navigate('Home'), 2000);
  }
  render() {
    return (
   <Container>
     <Content style={{backgroundColor:'#094872'}}>
       <View style={{marginTop:'30%',alignContent:'center',alignItems:'center'}}>
  <Text style={{fontSize:35,color:'#fff'}}>HarrisonPublisher</Text>
     <View style={{marginTop:'50%'}}><Spinner color='#ffb400' /><Text style={{ color: '#fff',textAlign:'center' }}>Loading....</Text></View>
      </View>
       </Content>
     </Container>
    );
  }
}

