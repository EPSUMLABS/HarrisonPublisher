import React, { Component } from 'react';
import { View, Text, Image, TouchableOpacity, Dimensions, StatusBar, 
AsyncStorage, Alert, ImageBackground,BackHandler,ScrollView, } from 'react-native';
import {
    Container, Header, Left, Body, Right, Icon, Button, Title, Footer, FooterTab, Content, Thumbnail, List, ListItem, Tab, Tabs, ScrollableTab,
    Separator,Badge,
    Item, Input, Spinner,Card, CardItem,
} from 'native-base';
import ImageSlider from 'react-native-image-slider';
import { Grid } from "react-native-easy-grid";
const screenwidth = Dimensions.get('window').width;
const screenHeight = Dimensions.get('window').height;
import urldetails from '../config/endpoints.json';
var SQLite = require('react-native-sqlite-storage');
let db = SQLite.openDatabase({name: 'test.db', createFromLocation : "~Harrison.db", location: 'Library'}, this.openCB, this.errorCB);
export default class Home extends Component {
  constructor(props) {
    super(props);
    this.state = {
        bannersdata:[],
        image1:'',
        image2:'',
        image3:'',
        image4:'', 
        id:'',      
    };
  }
  onBackPress = () => {
    BackHandler.exitApp()
  };

  componentWillMount(){
    fetch(urldetails.base_url+urldetails.url["banner"]) 
    .then((response)=>response.json())
    .then((jsondata)=>{
        console.log(jsondata);
        if (jsondata.status == "success") {
          console.log(jsondata.banner[0].ban1);
          this.setState({ image1: jsondata.banner[0].ban1,
            image2: jsondata.banner[0].ban2,
            image3: jsondata.banner[0].ban3,
            image4: jsondata.banner[0].ban4});
       
          console.log(urldetails.image + this.state.image1)
       }
       else {
         alert("Please try again..")
         this.setState({ spinner: false })
       }
    })
this.fetchcartitem()
} 
fetchcartitem() {
  db.transaction((tx) => {
          tx.executeSql('SELECT * FROM cart', [], (tx, results) => {
              console.log("Query completed");
              lcart = [];
              item = 0;
               t = 0;
              var len = results.rows.length; 
             for (let i = 0; i < len; i++) {
                  let row = results.rows.item(i);
                  lcart.push(row);
                  t = t + parseInt(row.qty * row.p_price);
                  item = i + 1;
                  this.setState({ cart: lcart, t_item: item, total: t })
                  console.log(this.state.cart) 
             }   
          });
    })
};
     render() {
      console.log(urldetails.image + this.state.image1)
      const images = [
        // http://harrison.epsumlabs.com/harrison/production/banner/bookbanner1.jpg,
        urldetails.image + this.state.image1,
        urldetails.image + this.state.image2,
        urldetails.image + this.state.image3,
        urldetails.image + this.state.image4
      ];
          var items = [
            'Books','Magazines','Audio Book','Video',
          ];
        return (
        <Container>
                <Header style={{ backgroundColor: '#094872' }} hasTabs>
                    <Left>
                        <TouchableOpacity onPress={()=>this.props.navigation.openDrawer()}>
                            <Icon name='ios-menu' style={{ fontSize: 25, color: 'white' }} />
                        </TouchableOpacity>
                    </Left>
                    <Body><Title>Home</Title></Body>
                    <Right>
                    <Badge style={{ backgroundColor: 'red',borderRadius: 25,width: 22, height: 22,marginBottom:8 }}>
            <Text style={{ color: 'white',marginLeft:2 }}>{this.state.t_item}</Text>
                   </Badge>  
                    <Icon name='ios-cart' style={{ fontSize: 25, color: 'white' }} onPress={()=>this.props.navigation.navigate("Cart")} />
                      </Right>
                </Header>
                <StatusBar backgroundColor="#1a1a1a" barStyle="light-content" />
                <Content style={{backgroundColor:'#fff'}}>
                  <View style={{alignContent:'center',alignItems:'center'}}>
               <View style={{width:screenwidth-10,height:screenHeight/3,marginTop:10,marginLeft:10,marginRight:10,
            alignContent:'center',alignItems:'center'}}>
               <ImageSlider autoPlayWithInterval={2000}
                    images={images}
                 style={{width:screenwidth-10}} />
                   </View>
                   <Grid  >
                   <TouchableOpacity onPress={()=>this.props.navigation.navigate(("Books"),
                                {id:1})}
                    style={{height:(Dimensions.get("window").height/4)-30,width:(Dimensions.get("window").width/2)-20,backgroundColor:'#f2daeb',
                    justifyContent:"center",alignContent:'center',alignItems:'center',elevation:8,margin:10,borderWidth:2,borderColor:'#e6e6e6'}}>
                    <Image
                    style={{resizeMode:'contain',height:100,width:100}}
                    source={require('../assets/bookicon.png')}/>
                      <Text style={{color:"#1532d6",fontWeight:"bold",fontSize:20}}>Books</Text>
                    </TouchableOpacity>
                    <TouchableOpacity onPress={()=>this.props.navigation.navigate("Magazines")}
                    style={{height:(Dimensions.get("window").height/4)-30,width:(Dimensions.get("window").width/2)-20,backgroundColor:'#d2ecef',alignItems:"center",
                    justifyContent:"center",elevation:8,margin:10,borderWidth:2,borderColor:'#e6e6e6'}}>
                   <Image
                    style={{resizeMode:'contain',height:100,width:100}}
                    source={require('../assets/magazineicon.png')}/>
                      <Text style={{color:"#c11744",fontWeight:"bold",fontSize:20}}>Magazines</Text>
                    </TouchableOpacity>
                     </Grid>
                     <Grid >
                   <TouchableOpacity onPress={()=>this.props.navigation.navigate("Audio")}
                    style={{height:(Dimensions.get("window").height/4)-30,width:(Dimensions.get("window").width/2)-20,backgroundColor:'#e8efd2',alignItems:"center",
                    justifyContent:"center",elevation:8,margin:10,borderWidth:2,borderColor:'#e6e6e6'}}>
                   <Image
                    style={{resizeMode:'contain',height:100,width:90}}
                    source={require('../assets/audioicon.png')}/>
                      <Text style={{color:"#16bc32",fontWeight:"bold",fontSize:20}}>Audio</Text>
                    </TouchableOpacity>
                    <TouchableOpacity onPress={()=>this.props.navigation.navigate("Video")}
                    style={{height:(Dimensions.get("window").height/4)-30,width:(Dimensions.get("window").width/2)-20,backgroundColor:'#e0c3af',alignItems:"center",
                    justifyContent:"center",elevation:8,margin:10,borderWidth:2,borderColor:'#e6e6e6'}}>
                   <Image
                    style={{resizeMode:'contain',height:100,width:90}}
                    source={require('../assets/videoicon.png')}/>
                      <Text style={{color:"#1835db",fontWeight:"bold",fontSize:20}}>Video</Text>
                    </TouchableOpacity>
                     </Grid>
                     
            </View>   
        </Content> 
        </Container>
    );
}
}
