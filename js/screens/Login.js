
import React, { Component } from 'react';
import {
  Platform,
  StyleSheet,
  Text,
  View,ImageBackground,TouchableOpacity,Image,Dimensions,AsyncStorage
} from 'react-native';
import { Container, Header, Content,Left,Body,Title,Right,Item,Input,Thumbnail,Icon} from 'native-base';
import { validateEmail,validatePassword  } from '../config/validate';
import urldetails from '../config/endpoints.json';
import Ionicons from 'react-native-vector-icons/Ionicons';
const screenwidth=Dimensions.get('window').width;
const screenHeight=Dimensions.get('window').height;
export default class Login extends Component {
  constructor()
  {
    super();
    this.state={
      showop:true,
    email:'',
    password:'',
    userid:'',
    cid:'',
    spinner: false,
    }
  }
  onBackPress = () => {
    this.props.navigation.navigate("Home");
    return true;
  };

  loginUser(){
    this.setState({spinner:true})
    var formData= new FormData();
    formData.append('cemail',this.state.email);
    formData.append('cpassword',this.state.password);
    console.log(formData)
    fetch(urldetails.base_url+urldetails.url["login"],{
      method:"POST",
      body:formData
    }) .then((response)=>response.json())
    .then((jsondata)=>{
        console.log(jsondata);
        if (jsondata.status == "success") {
          console.log('',jsondata.message)
          this.props.navigation.navigate("Home")
          var userdetails={
            userid:jsondata.userdata[0].cid,
           username:jsondata.userdata[0].cname,
           address:jsondata.userdata[0].caddress,
           contact:jsondata.userdata[0].ccontactno,
            email:this.state.email,
            password:this.state.password,   
          }
         
          AsyncStorage.setItem('user',JSON.stringify(userdetails));
          console.log(userdetails)
    
          }
          else {
            
            this.setState({ spinner: false })
          }
    });
}
  onBackPress = () => {
    this.props.navigation.navigate("Home");
    return true;
  };

  render() {
    return (
      <Container>
          <Content style={{backgroundColor:'#e8e8e8'}}>
              <View style={{alignItems:'center',justifyContent:'center'}}>
              <Text style={{fontSize:35,color:'#094872',marginTop:'30%'}}>HarrisonPublisher</Text>
         <View style={{ flexDirection:'row',flexGrow: 1, alignItems: 'center', justifyContent: 'center',marginTop:'10%' }}>
         <Item  regular style={{width:350,backgroundColor:'rgba(255,255,255,1.0)',marginTop:7 }}>
         <Thumbnail square source={{uri:'http://simpleicon.com/wp-content/uploads/user1.png'}}   style={{marginLeft:10,width:20,height:20}}/>
            <Input placeholder='Email' placeholderTextColor='#999999' onChangeText={(email)=>this.setState({email:email})} autoCapitalize="none" autoCorrect={false}  />
          </Item>
          </View>
          <View style={{ flexDirection:'row',flexGrow: 1, alignItems: 'center', justifyContent: 'center',marginTop:5 }}>
          <Item  regular style={{width:350,backgroundColor:'rgba(255,255,255,1.0)',marginTop:7 }}>
          <Icon name='lock' style={{marginLeft:3,width:40,height:30}}/>
            <Input placeholder='Password' placeholderTextColor='#999999' secureTextEntry={this.state.showop} onChangeText={(password=>this.setState({password:password}))} autoCorrect={false}/>
            <TouchableOpacity>
                    <Ionicons name="ios-eye-outline" onPress={()=>this.setState({showop:!this.state.showop})} style={{ fontSize: 35, color: '#000',padding:10 }} />
            </TouchableOpacity>

          </Item>
      </View>
      <View style={{marginTop:20,marginBottom: 20,justifyContent: 'center',alignItems: 'center',}}>
       <TouchableOpacity onPress={() => this.loginUser()} style={{width: screenwidth-60,height: 45,backgroundColor: "#3db5b1",borderRadius:10,borderColor: 'rgba(255, 255, 255, 0.5)',
  alignItems: "center",justifyContent: 'center',elevation: 8,shadowColor: 'black',shadowOffset: {width: 3,height: 3},shadowOpacity: 0.5,shadowRadius: 5}}  >
            <Text style={{ color: "white", fontSize: 20 }}>LOGIN</Text>  
          </TouchableOpacity>
          <TouchableOpacity   style={{width: screenwidth-60,height: 45,backgroundColor: "#3b5998",borderRadius:10,
          marginTop:10,borderColor: 'rgba(255, 255, 255, 0.5)',
  alignItems: "center", justifyContent: 'center', elevation: 8, flexDirection:'row', shadowColor: 'black',shadowOffset: {width: 3,height: 3},shadowOpacity: 0.5,shadowRadius: 5}} 
  >
            <Icon name='logo-facebook'style={{ color: "white", fontSize: 20,paddingRight:5 }} />
            <Text style={{ color: "white", fontSize: 20 }}>Login</Text>
          </TouchableOpacity>
          </View>
          <View style={{flexDirection:'row'}}>
          <TouchableOpacity style={{marginTop:5,paddingRight:25}} onPress={() =>this.props.navigation.navigate("Forgotpassword")}>
        <Text style={{color:'#000000',fontSize:20}}>Forgot Password</Text>
        </TouchableOpacity>
        <TouchableOpacity style={{marginTop:5,paddingLeft:25}} onPress={() =>this.props.navigation.navigate("Signup")}>
        <Text style={{color:'#000000',fontSize:20}}>Create account</Text>
        </TouchableOpacity>
        </View>
         
         
    
          
      </View>
      </Content>
     </Container>
    );
  }
}


