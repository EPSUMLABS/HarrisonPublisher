
import React, { Component } from 'react';
import {
  Platform,
  StyleSheet,
  Text,
  View,ImageBackground,TouchableOpacity,Image,Dimensions,StatusBar,ScrollView,
} from 'react-native';
import { Container, Header, Content,Left,Body,Title,Right,Item,Input,Thumbnail,Icon,Tab,Tabs,TabHeading,
  Card,ScrollableTab,List,CardItem,CheckBox,Badge,Spinner,
Footer,FooterTab, ListItem} from 'native-base';
import urldetails from '../config/endpoints.json';
import ReadMore from 'react-native-read-more-text';
const screenwidth=Dimensions.get('window').width;
const screenHeight=Dimensions.get('window').height;
var SQLite = require('react-native-sqlite-storage');
import Modal from "react-native-simple-modal";
let db = SQLite.openDatabase({ name: 'test.db', createFromLocation: "~Harrison.db", location: 'Library' }, this.openCB, this.errorCB);
export default class Bookdetails extends Component {
  state={
    open: false,
    details: this.props.navigation.state.params.details,
    modalimage:'',
    spinner:true,
  }
  componentWillMount(){
    this.fetchcartitem()
    this.setState({spinner:false})
  
  }
   addtocart( p_id,name,price,p_hardcopy,p_softcopy) {
  // this._onpressadd(itemid);
     db.transaction((tx) => {
       tx.executeSql('insert into cart(p_id,p_name,p_price,qty,p_hardcopy,p_softcopy) values(?,?,?,?,?,?)', [p_id,name,price,1,p_hardcopy,p_softcopy], (tx, results) => {
         console.log("added to cart") 
       });
       this.fetchcartitem()
   });
   }
   modalDidOpen = () => console.log("Modal did open.");
   modalDidClose = () => {
     this.setState({ open: false });
     console.log("Modal did close.");
   };
   moveUp = () => this.setState({ offset: -100 });
   resetPosition = () => this.setState({ offset: 0 });
   openModal = () => this.setState({ open: true });
   closeModal = () => this.setState({ open: false });
   modaImage(modalimage) {
    this.setState({ modalimage: modalimage,
    open:true })
  }
  fetchcartitem() {
    db.transaction((tx) => {
            tx.executeSql('SELECT * FROM cart', [], (tx, results) => {
                console.log("Query completed");
                lcart = [];
                item = 0;
                 t = 0;
                var len = results.rows.length; 
               for (let i = 0; i < len; i++) {
                    let row = results.rows.item(i);
                    lcart.push(row);
                    t = t + parseInt(row.qty * row.p_price);
                    item = i + 1;
                    this.setState({ cart: lcart, t_item: item, total: t })
                    console.log(this.state.cart) 
               }   
            });
      })
  };


  render() {
  return (
      <Container>
           <Header style={{ backgroundColor: '#094872' }} hasTabs>
                    <Left>
                        <TouchableOpacity onPress={()=>this.props.navigation.openDrawer()}>
                            <Icon name='ios-menu' style={{ fontSize: 25, color: 'white' }} />
                        </TouchableOpacity>
                    </Left>
                    <Body><Title>Details</Title></Body>
                    <Right>
                    <Badge style={{ backgroundColor: 'red',borderRadius: 25,width: 22, height: 22,marginBottom:8 }}>
            <Text style={{ color: 'white',marginLeft:2 }}>{this.state.t_item}</Text>
          </Badge>  
                    <Icon name='ios-cart' style={{ fontSize: 25, color: 'white' }}  onPress={()=>this.props.navigation.navigate("Cart")}/>
                      </Right>
                </Header>
                <StatusBar backgroundColor="#1a1a1a" barStyle="light-content" />

          <Content style={{backgroundColor:'#e8e8e8'}}>
          {this.state.spinner ? <View style={{marginTop:screenHeight/3}}><Spinner color='#ffb400' /><Text style={{color:'#000',textAlign:'center'}}>Loading...</Text></View> : <View />}       
        <Card style={{height:screenHeight/3,marginTop:7}}>
          <View style={{flexDirection:'row'}}>
          <Image source={{uri: urldetails.image+this.state.details.image[0].sample_path}} style={{height:screenHeight/3.5, width:screenwidth-220,marginTop:15,marginLeft:10, }}/>
          <View style={{flexDirection:'column',marginLeft:30,marginTop:10}}>
                  <Text style={{fontSize:20,color:'#000'}}>{this.state.details.ptitle}</Text>
                  <Text style={{fontSize:15,paddingTop:5,color:'#000'}}>{this.state.details.pauther}</Text>
                  <Text style={{fontSize:15,}}>{this.state.details.ppublisher}</Text>
                  <Text style={{fontSize:15}}>{this.state.details.pisbnno}</Text>
                  {/* <Text style={{fontSize:15}}></Text> */}
                  <View style={{flexDirection:'row',marginTop:15}}>
                  {/* <TouchableOpacity  style={{ backgroundColor: '#dadee5', width: screenwidth / 3.5 ,borderWidth:2,borderColor:'#094872' }}> */}
                <View style={{ flexDirection: 'row', alignContent: 'center', justifyContent: 'center', padding: 10 }}>
                  {/* <FontAwesome style={{ color: '#fff', padding: 2,fontSize: 16 }} name="pencil-square-o" /> */}
                  {/* <Text style={{  padding: 2, fontSize: 15 }}></Text> */}
                  <Text style={{ color: '#000', padding: 2, fontSize: 15 }}>Rs.{this.state.details.pprice}</Text>
                  {/* <FontAwesome style={{ color: '#fff', paddingLeft: 2,fontSize: 16 }} name="pencil-square-o" /> */}
                </View>
              {/* </TouchableOpacity> */}
              </View>
                  </View>      
          </View>
          </Card> 
          <Card style={{flexDirection:'row',width:screenwidth,height:screenHeight/3.7,marginTop:2,alignContent:'center',alignItems:'center'}}>
  {
    this.state.details.image.map((u, i) => {
      return (
      
        <TouchableOpacity key={i} onPress={() => this. modaImage(u.sample_path)}
         style={{alignContent:'center',alignItems:'center',padding:5,elevation:8,borderWidth:1,borderColor:'#e6e6e6',backgroundColor:'#fff',
        height:screenHeight/5,marginLeft:2}} >
     <Image source={{uri: urldetails.image+u.sample_path}} style={{height:screenHeight/5, width:screenwidth/5,marginLeft:5 }} />
         
        </TouchableOpacity>
       
      );
    })
  }
</Card>  
 {/* <View style={{flexDirection:'row',backgroundColor:'#fff',marginTop:5}}>
          <Image source={{uri: urldetails.image+this.state.details.image[0].sample_path}} 
          style={{height:screenHeight/5.5, width:screenwidth-320,marginTop:3,marginLeft:3 }}/>
              <Image source={{uri: urldetails.image+this.state.details.image[1].sample_path}} style={{height:screenHeight/5.5, width:screenwidth-320,marginTop:3,marginLeft:5 }}/>
              <Image source={{uri: urldetails.image+this.state.details.image[2].sample_path}} style={{height:screenHeight/5.5, width:screenwidth-320,marginTop:3,marginLeft:5 }}/>
              <Image source={{uri: urldetails.image+this.state.details.image[3].sample_path}} style={{height:screenHeight/5.5, width:screenwidth-320,marginTop:3,marginLeft:5 }}/>
          </View>
          <View>
    </View> */}
          <List style={{marginTop:5,backgroundColor:'#fff'}}>
            <ListItem style={{flexDirection:'column'}}>
           <Text>ABOUT THE BOOK</Text>
           <ReadMore
            numberOfLines={2}
            onReady={this._handleTextReady}> 
            <Text style={{color:'black',fontSize:16}}>
             {this.state.details.pdescription}
            </Text>
          </ReadMore>
              </ListItem>
            </List>
      </Content>
      <Footer style={{paddingTop:0}}>
            <FooterTab style={{ backgroundColor: '#fff' }}>
              <TouchableOpacity onPress={()=>this.props.navigation.navigate('Payments')} 
               style={{ backgroundColor: '#094872', width: screenwidth / 2 - 0.2 }}>
                <View style={{ flexDirection: 'row', alignContent: 'center', justifyContent: 'center', padding: 10 }}>
                  {/* <FontAwesome style={{ color: '#fff', padding: 2,fontSize: 16 }} name="pencil-square-o" /> */}
                  <Text style={{ color: '#fff', padding: 2, fontSize: 16 }}>BUY</Text>
                  {/* <FontAwesome style={{ color: '#fff', paddingLeft: 2,fontSize: 16 }} name="pencil-square-o" /> */}
                </View>
              </TouchableOpacity>
              <TouchableOpacity   onPress={() => this.addtocart(this.state.details.pid,this.state.details.ptitle,this.state.details.pprice,
              this.state.details.p_hardcopy,this.state.details.p_softcopy  
              )}
               style={{ backgroundColor: '#094872', width: screenwidth / 2 - 0.2 }}>
                <View style={{ flexDirection: 'row', alignContent: 'center', justifyContent: 'center', padding: 10 }}>
                  <Icon style={{ color: '#ffb400', padding: 2 }} name="md-cart" />
                  <Text style={{ color: '#fff', padding: 2, fontSize: 16 }}>Add to Cart</Text>
                </View>
              </TouchableOpacity>
            </FooterTab>
          </Footer>
          <Modal style={{width:screenwidth/2,height:screenHeight/2}}
offset={0}
open={this.state.open}
modalDidOpen={this.modalDidOpen}
modalDidClose={this.modalDidClose}
style={{ alignItems: "center", }}
>
<View style={{ flexDirection: 'row', alignContent: 'center', justifyContent: 'center' }}>
<Left></Left>
<Right>
<TouchableOpacity style={{ backgroundColor: 'red', borderRadius: 25, width: 25, height: 25 }} onPress={() => this.setState({ open: false })} >
<Text style={{ color: '#fff', fontWeight: 'bold', padding: 2, textAlign: 'center' }}>X</Text>
</TouchableOpacity>
</Right>
</View>
<View style={{  alignContent: 'center', justifyContent: 'center' }}>
<Image source={{uri: urldetails.image+this.state.modalimage}} style={{height:screenHeight/2, width:screenwidth/2,marginLeft:'20%' }} />
          </View>
</Modal>
  
     </Container>
    );
  }
}


