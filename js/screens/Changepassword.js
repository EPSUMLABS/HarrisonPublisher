import React, { Component } from 'react';
import {  View, Text,TouchableOpacity,Dimensions,StatusBar,AsyncStorage,Alert,ImageBackground,BackHandler} from 'react-native';
import { Container, Header, Left, Body,Icon, Title,Thumbnail,Item, Input,Spinner} from 'native-base';

const screenwidth=Dimensions.get('window').width;
const screenHeight=Dimensions.get('window').height;
import urldetails from '../config/endpoints.json';
export default class Changepassword extends Component {
  constructor() {
    super();
    this.state = {
      oldpassword: '',
      oldpasswordvalid: '',
      newpassword: '',
      useridval: '',
      Spinner: false,
      
    }
  }
  componentWillMount() {
  AsyncStorage.getItem('user')
  .then((value) => {
    if (value != null || value != undefined) {
      const values = JSON.parse(value)
      console.log('user available' + values.userid + '' + values.password)
      this.setState({ useridval: values.userid })
      this.setState({ oldpasswordvalid: values.password })
      console.log(this.state.useridval)
      console.log(this.state.oldpasswordvalid)
    }
  })
}
changePassword() {
  if (this.state.oldpassword == '' || this.state.newpassword == '') {
    alert("All Fields are required...");
  }
  // } else if (!validatePassword(this.state.newpassword)) {
  //   alert('Password should contain one uppercase letter,one digit and should be of 8 characters ..')
  // }
  else if (this.state.oldpassword != this.state.oldpasswordvalid) {
    alert("Old password is incorrect..");
  }
  else {
    this.setState({ spinner: true })
    var formData = new FormData();
    formData.append('cid', this.state.useridval);
    formData.append('cpassword', this.state.newpassword);
    console.log(formData)
    fetch(urldetails.base_url + urldetails.url["cpassword"], {
      method: "POST",
      body: formData
    })
      .then((response) => response.json())
      .then((jsondata) => {
        console.log(jsondata)
        if (jsondata.status == "Sucess") {
          Alert.alert('', jsondata.msg)
          this.props.navigation.navigate("Home")
        }
        else if (jsondata.status == "Fail") {
          Alert.alert('', jsondata.msg)
        }
        this.setState({ spinner: false })
      })
  }
}

  render() {
    return (
      <Container>
     
          <Header style={{ backgroundColor: "#094872" }} hasTabs>
            <Left>
              <TouchableOpacity transparent onPress={() => this.props.navigation.openDrawer()}>
                <Icon name='ios-menu' style={{ fontSize: 25, color: 'white' }} />
              </TouchableOpacity>
            </Left>
            <Body><Title></Title></Body>
          </Header>
          <StatusBar backgroundColor="#000" barStyle="light-content" />
          <View style={{ padding: 40, alignItems: 'center' }}>
   
            <Text style={{ textAlign: 'center', color: '#094872', fontSize: 22, fontWeight: 'bold', marginTop: 20 }}>CHANGE PASSWORD</Text>
            <View style={{ flexGrow: 1, alignItems: 'center', justifyContent: 'center' }}>
            <Item regular style={{width:screenwidth-40,alignItems:'center',backgroundColor:'#fff',marginTop:10,borderRadius:5,borderColor: '#c8c8c8'}}>
                <Input style={{ fontSize: 16, color: 'black' }} placeholder='OLD PASSWORD' placeholderTextColor='#000' secureTextEntry={true} textAlign='center' 
                onChangeText={(oldpassword) => this.setState({ oldpassword: oldpassword })} autoCorrect={false} />
              </Item>
              <Item regular style={{width:screenwidth-40,alignItems:'center',backgroundColor:'#fff',marginTop:10,borderRadius:5,borderColor: '#c8c8c8'}}>
                <Input style={{ fontSize: 16, color: 'black' }} placeholder='NEW PASSWORD' placeholderTextColor='#000' secureTextEntry={true} textAlign='center'
                 onChangeText={(newpassword => this.setState({ newpassword: newpassword }))} autoCorrect={false} />
              </Item>
              <View style={{ height: 40, marginTop: 25, marginBottom: 20, justifyContent: 'center', alignItems: 'center', }}>
                <TouchableOpacity onPress={() => this.changePassword()} style={{
                  width: screenwidth - 40, height: 50, backgroundColor: "#094872", borderRadius: 5,
                  alignItems: "center", borderColor: 'rgba(255, 255, 255, 0.3)', justifyContent: 'center', elevation: 8, shadowColor: 'black', shadowOffset: { width: 3, height: 3 }, shadowOpacity: 0.5, shadowRadius: 5
                }}>
                  <Text style={{ color: "white", fontSize: 20 }}>SAVE</Text>
                </TouchableOpacity>
              </View>
            </View>
  
          </View>
     
      </Container>
  );
}
}
