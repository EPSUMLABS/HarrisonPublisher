import React, { Component } from 'react';
import { View, Text, Dimensions, AsyncStorage, TouchableOpacity, StatusBar, Alert, BackHandler } from 'react-native';
import { Container,  Content,  Spinner,  Item,  Input,  Form,  Header,  Body,  Title,  Label,  Left,  Icon} from 'native-base';
const screenwidth=Dimensions.get('window').width;
const screenHeight=Dimensions.get('window').height;
import urldetails from '../config/endpoints.json';
import Modal from 'react-native-simple-modal';
var SQLite = require('react-native-sqlite-storage');
import { validateEmail, validatePhone, validateName, validatePassword ,ValidatePincode } from '../config/validate';
let db = SQLite.openDatabase({name: 'test.db', createFromLocation : "~biggies.db", location: 'Library'}, this.openCB, this.errorCB);
export default class Shipping extends Component {
  state={
    // total:this.props.navigation.state.params.total,
    // time:this.props.navigation.state.params.time,
    open: false,
    cart: this.props.navigation.state.params.cart, 
    total: this.props.navigation.state.params.total, 
    name:"",
    contactno:"",
    email:"",
    city:"",
    address:"",
    district:"",
    state:"",
    pincode:"",
    uid:"",
    spinner:false,
   p_id:"", 
    qty:"",
    shipping_id:'',
    
  }
  componentWillMount(){
//     db.transaction((tx) => {
//       tx.executeSql('SELECT * FROM cart', [], (tx, results) => {
//           console.log("Query completed");
//           lcart = [];
//           item = 0;
//            t = 0;
//           var len = results.rows.length; 
//          for (let i = 0; i < len; i++) {
//               let row = results.rows.item(i);
//               lcart.push(row);
//               t = t + parseInt(row.qty * row.p_price);
//               item = i + 1;
//               this.setState({ cart: lcart, t_item: item, total: t,p_id:row.p_id,qty:row.qty })
//                console.log(this.state.p_id) 
    
//          }   
     

//       });
// })
       
   
  }
  modalDidOpen = () => console.log("Modal did open.");
  modalDidClose = () => {
      this.setState({ open: false });
      console.log("Modal did close.");
    };
    openModal = () => this.setState({ open: true });
  closeModal = () => this.setState({ open: false });

  verif(){
    this.setState({spinner:true})
    if (this.state.name == "") {
      alert('Please enter your Name ..')
    }
    else if (this.state.contactno == "") {
        alert('Phone number field cannot be blank !')
      }
  
    else if (this.state.email == "") {
      alert('Email field cannot be blank !')
    }

  
    else if (this.state.address == "") {
      alert('Address field cannot be blank !')
    }
    else if (this.state.city == "") {
      alert('City field cannot be blank !')
    }
    else if (this.state.district == "") {
        alert('district field cannot be blank !')
      }
    else if (this.state.state == "") {
      alert('state field cannot be blank !')
    }
    else if (this.state.pincode == "") {
      alert('Pincode field cannot be blank !')
    }
    else if (!ValidatePincode(this.state.pincode)) {
      alert('Zip Code can not be more or less than 6 digits !')
    }
    else{
        this.setState({spinner:true})
        p_id="";
        qty="";
        p_hardcopy="";
        p_softcopy="";
        this.state.cart.map((mycart,i)=>{
          console.log(mycart)
          if(p_id.length>0){
            p_id=p_id+","+mycart.p_id
            qty=qty+","+mycart.qty
            p_hardcopy=p_hardcopy+","+mycart.p_hardcopy
            p_softcopy=p_softcopy+","+mycart.p_softcopy
          }
          else{
            p_id=mycart.p_id
            qty=mycart.qty
            p_hardcopy=mycart.p_hardcopy
            p_softcopy=mycart.p_softcopy
          }
         })
        var formData = new FormData();
        formData.append('ship_name', this.state.name);
        formData.append('ship_contact', this.state.contactno);
        formData.append('ship_address', this.state.address);
        formData.append('ship_city', this.state.city);
        formData.append('ship_state', this.state.state);
        formData.append('ship_district', this.state.district);
        formData.append('ship_pincode', this.state.pincode);
        formData.append('ship_email', this.state.email);
        formData.append('product_id', ""+p_id+"");
        formData.append('quantity',  ""+qty+"");
        formData.append('pro_softcopy',  ""+p_softcopy+"");
        formData.append('pro_hardcopy',  ""+p_hardcopy+"");
        console.log(formData);
        fetch(urldetails.base_url+urldetails.url["shipping"],{
          method: "POST",
          body: formData,
        })
    .then((response)=>response.json())
    .then((jsondata)=>{
      console.log(jsondata)
      if(jsondata.status=="success"){
        console.log(jsondata.message)
        var shipdetails={
          ship_email:this.state.email,  
          }
          AsyncStorage.setItem('ship_details',JSON.stringify(shipdetails));
          console.log(shipdetails)
       this.props.navigation.navigate("Confirmorder",{
         shipping_id:jsondata.shipping_id,
         name:this.state.name,
         email:this.state.email,
         contactno:this.state.contactno,
         address:this.state.address,
         city:this.state.city,
         state:this.state.state,
         district:this.state.district,
         pincode:this.state.pincode,
         total:this.state.total,
       })
{

}      
      }
      else{
        this.setState({ spinner: false })
      }
    }
    )
    .catch((error) => {
      this.setState({ spinner: false})
      console.log(error)
    }); 
       }    
  }
    render() {
        return (
        <Container>
             <Header style={{backgroundColor:'#094872'}}>
             <Left>
               <TouchableOpacity style={{padding:5}} onPress={()=>this.props.navigation.navigate('Cart')}>
                        <Icon name='arrow-back' style={{ fontSize: 25, color: 'white' }} />
                        </TouchableOpacity>
                    </Left> 
             <Body style={{alignContent:'center'}}><Title>Shipping Address</Title></Body>
             </Header>
        <StatusBar backgroundColor="#000" barStyle="light-content" />
        <Content style={{ backgroundColor: '#efefef' }}>
        
          <View style={{ flexGrow: 1, alignItems: 'center', justifyContent: 'center',paddingTop:25 }}>
            <Item regular style={{ width:screenwidth-40, alignItems: 'center', backgroundColor: '#fff',marginTop:10,borderRadius: 5 }}>
                <Input placeholder='Name' 
                onChangeText={(name) => this.setState({ name: name })}
                value={this.state.name}/>
              </Item>
              <Item regular style={{ width:screenwidth-40, alignItems: 'center', backgroundColor: '#fff',marginTop:10,borderRadius: 5  }}>
                <Input placeholder='contactno'
                keyboardType={"phone-pad"}
                onChangeText={(contactno) => this.setState({ contactno: contactno })}
                value={this.state.contactno} />
              </Item>

              <Item regular style={{ width:screenwidth-40, alignItems: 'center', backgroundColor: '#fff',marginTop:10,borderRadius: 5  }}>
                <Input placeholder='Email' 
                keyboardType={"email-address"}
                onChangeText={(email) => this.setState({ email: email })}
                value={this.state.email}/>
              </Item>
              <Item regular style={{ width:screenwidth-40, alignItems: 'center', backgroundColor: '#fff',marginTop:10,borderRadius: 5  }}>
                <Input placeholder='Address' 
                onChangeText={(address)=>this.setState({address:address})}/>
              </Item>
              <Item regular style={{ width:screenwidth-40, alignItems: 'center', backgroundColor: '#fff',marginTop:10,borderRadius: 5  }}>
                <Input placeholder='City' 
                onChangeText={(city)=>this.setState({city:city})}/>
              </Item>
              <Item regular style={{ width:screenwidth-40, alignItems: 'center', backgroundColor: '#fff',marginTop:10,borderRadius: 5  }}>
                <Input placeholder='District' 
                onChangeText={(district)=>this.setState({district:district})}/>
              </Item>
              <Item regular style={{ width:screenwidth-40, alignItems: 'center', backgroundColor: '#fff',marginTop:10,borderRadius: 5  }}>
                <Input placeholder='State' 
                onChangeText={(state)=>this.setState({state:state})}/>
              </Item>
              <Item regular style={{ width:screenwidth-40, alignItems: 'center', backgroundColor: '#fff',marginTop:10,borderRadius: 5  }}>
                <Input placeholder='ZipCode'
                keyboardType={"phone-pad"} 
                onChangeText={(pincode)=>this.setState({pincode:pincode})}/>
              </Item>
              </View>
            <View style={{ height: 40, marginTop: 25, marginBottom: 20, justifyContent: 'center', alignItems: 'center', }}>
              <TouchableOpacity style={{
                width: screenwidth - 40, height: 50, backgroundColor: "#094872",
                alignItems: "center", justifyContent: 'center',borderRadius: 5, elevation: 8, shadowColor: 'black', shadowOffset: { width: 3, height: 3 }, shadowOpacity: 0.5, shadowRadius: 5
              }}
                onPress={() => this.verif()}>
                <Text style={{ color: "white", fontSize: 16 }}>CHECKOUT</Text>
              </TouchableOpacity>
          </View>
        </Content>
        <Modal
                    offset={0}
                    open={this.state.open}
                    modalDidOpen={this.modalDidOpen}
                    modalDidClose={this.modalDidClose}
                    style={{ alignItems: "center" }}
                    closeOnTouchOutside={false} 
                    disableOnBackPress={true}
                >
                    <View >

                       <Text>hello</Text>
                    </View>
                </Modal>

     
      </Container>
        );
    }}
